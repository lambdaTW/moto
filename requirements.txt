# wheel
wheel

Django==2.0.6
django-bootstrap4==0.0.6
pytz==2018.5

# filter
django-filter==1.1.0

# psql
psycopg2-binary
