from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages as m

from moto.filters import MotoFilter
from moto.models import Moto
from moto.forms import CreateMotoForm, MotoModelForm
from custom.models import Custom


@login_required
def index(request):
    _filter = MotoFilter(
        request.GET or None,
        queryset=Moto.objects.all()
    )
    paginator = Paginator(_filter.qs.order_by('-time'), 10)
    page = request.GET.get('page')
    create_form = CreateMotoForm()
    try:
        page = paginator.page(page)
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    return render(request, 'moto/index.html', {'filter': _filter, 'page': page, 'create_form': create_form})


@login_required
def new(request):
    form = CreateMotoForm(request.POST or None, request.FILES or None)
    if not form.is_valid():
        return render(request, 'moto/new.html', {'form': form})
    try:
        moto = Moto.objects.create(pk=form.cleaned_data['id'].upper())
    except:
        m.warning(request, '車牌已經存在')
        return render(request, 'moto/new.html', {'form': form})
    if form.cleaned_data['phone']:
        owner, _ = Custom.objects.get_or_create(
            **{k: v for k, v in form.cleaned_data.items() if k in ['phone', 'name']})
        moto.owner = owner
        moto.save()
    return redirect('index')


@login_required
def edit(request, pk):
    moto = get_object_or_404(Moto, pk=pk)
    form = MotoModelForm(request.POST or None,
                         request.FILES or None, instance=moto)
    if form.is_valid():
        moto = form.save(request.user)
        if form.cleaned_data['phone']:
            owner, _ = Custom.objects.get_or_create(phone=form.cleaned_data['phone'])
            if form.cleaned_data['name']:
                owner.name = form.cleaned_data['name']
                owner.save()
            moto.owner = owner
            moto.save()
        return redirect('edit', pk)

    return render(request, 'moto/edit.html', {'form': form})
