from django_filters import FilterSet, CharFilter, NumberFilter
from .models import Moto

class MotoFilter(FilterSet):
    id = CharFilter(label='車牌號碼', field_name='id', lookup_expr='startswith')
    owner__phone = CharFilter(label='客戶手機', field_name='owner__phone', lookup_expr='startswith')

    class Meta:
        model = Moto
        fields = ['id', 'owner__phone']