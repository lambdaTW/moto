from django.contrib import admin
from .models import Supplier, MotoModel, Moto


admin.site.register(Supplier)
admin.site.register(MotoModel)
admin.site.register(Moto)