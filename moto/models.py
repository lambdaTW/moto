from django.db import models
from custom.models import Custom


class Supplier(models.Model):
    name = models.CharField('英文名稱', max_length=30)
    chinese_name = models.CharField('中文名稱', max_length=20)

    class Meta:
        verbose_name = '車商'
        verbose_name_plural = '車商'

    def __str__(self):
        return self.chinese_name + ' (' + self.name + ')'


class MotoModel(models.Model):
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    name = models.CharField('型號', max_length=30)

    class Meta:
        verbose_name = '車款'
        verbose_name_plural = '車款'

    def __str__(self):
        return '-'.join([self.supplier.name, self.name])


class Moto(models.Model):
    id = models.CharField('車牌號碼', max_length=20, primary_key=True)
    owner = models.ForeignKey(Custom, verbose_name='車主', blank=True, null=True, on_delete=models.CASCADE)
    model = models.ForeignKey(MotoModel, verbose_name='車款', blank=True, null=True, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = '機車'
        verbose_name_plural = '機車'

    def __str__(self):
        return self.id
