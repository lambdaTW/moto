from django.forms import Form, ModelForm, formset_factory, fields
from .models import Moto


class CreateMotoForm(Form):
    id = fields.CharField(label='車牌號碼')
    phone = fields.CharField(label='車主號碼', required=False)
    name = fields.CharField(label='車主姓名', required=False)


class MotoModelForm(ModelForm):
    phone = fields.CharField(label='車主號碼', required=False)
    name = fields.CharField(label='車主姓名', required=False)

    class Meta:
        model = Moto
        exclude = ['time']