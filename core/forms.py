from django import forms


class DeleteConfirmForm(forms.Form):
    check = forms.BooleanField(label='確定刪除')
