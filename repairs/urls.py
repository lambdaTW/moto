from django.urls import path
from . import views


app_name = 'repair'

urlpatterns = [
    path('', views.index, name='index'),
    path('report/', views.report, name='report'),
    path('new/', views.new, name='new'),
    path('edit/<int:pk>/', views.edit, name='edit'),
    path('delete/<int:pk>/', views.delete, name='delete'),
    path('add/<int:pk>/', views.add, name='add'),

    path('items/<int:pk>/', views.items_detail, name='items_detail'),
    path('items/new/', views.items_new, name='items_new'),
    path('items/edit/<int:pk>/', views.items_edit, name='items_edit'),
    path('items/delete/<int:pk>/', views.items_delete, name='items_delete'),

    path('categories/', views.categories_index, name='categories_index'),
    path('categories/<int:pk>/', views.categories_detail, name='categories_detail'),
    path('categories/new/', views.categories_new, name='categories_new'),
    path('categories/edit/<int:pk>/', views.categories_edit, name='categories_edit'),
    path('categories/delete/<int:pk>/', views.categories_delete, name='categories_delete'),
]
