from django.forms import Form, ModelForm, formset_factory, fields, inlineformset_factory
from .models import Recorder, Detail, Item, Category
from custom.models import Custom


class CategoryCreateForm(ModelForm):
    class Meta:
        model = Category
        fields = '__all__'


class RootItemCreateForm(ModelForm):
    class Meta:
        model = Item
        exclude = ['root', ]


class RootItemUpdateForm(ModelForm):
    name = fields.CharField(label="項目名稱", required=False)

    def clean_name(self):
        data = self.cleaned_data['name']
        if data == '':
            return self.instance.name
        return data

    class Meta:
        model = Item
        exclude = ['root', ]


class ItemCreateForm(ModelForm):
    class Meta:
        model = Item
        fields = '__all__'


class ItemUpdateForm(RootItemUpdateForm):
    class Meta:
        model = Item
        fields = '__all__'


class ReportSearchForm(Form):
    year = fields.IntegerField(label="年", required=False)
    month = fields.IntegerField(label="月", required=False)
    day = fields.IntegerField(label="日", required=False)


class CreateRecorderForm(Form):
    id = fields.CharField(label='車牌號碼')


RepairFormSet = inlineformset_factory(
    Recorder, Detail, exclude=['item'], extra=1)
