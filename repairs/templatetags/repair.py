from django import template


register = template.Library()


@register.filter
def sum_recorder(recorder):
    return sum([detail.price*detail.count for detail in recorder.detail_set.all()])