from django.db import models
from django.core.validators import MinValueValidator
from moto.models import Moto
from custom.models import Custom


class Category(models.Model):
    name = models.CharField('類別名稱', max_length=20)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '維修類別'
        verbose_name_plural = '維修類別'


class Item(models.Model):
    name = models.CharField('項目名稱', max_length=30)
    price = models.IntegerField('價格', validators=[
        MinValueValidator(1)
    ])
    root = models.ForeignKey('self', verbose_name='隸屬於',
                             blank=True, null=True, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, verbose_name='大類別',on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '維修項目'
        verbose_name_plural = '維修項目'


class Recorder(models.Model):
    moto = models.ForeignKey(Moto, on_delete=models.CASCADE)
    custom = models.ForeignKey(Custom, blank=True, null=True, on_delete=models.CASCADE)
    time = models.DateField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    km = models.IntegerField('里程', default=0)

    class Meta:
        verbose_name = '維修單'
        verbose_name_plural = '維修單'

    def __str__(self):
        return '-'.join([self.moto.id, str(self.time)])


class Detail(models.Model):
    recorder = models.ForeignKey(Recorder, verbose_name='紀錄', on_delete=models.CASCADE)
    item = models.ForeignKey(Item, verbose_name='物件', on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField('項目名稱', default=None,
                            blank=True, null=True, max_length=30)
    price = models.IntegerField('價格', default=0,
                                validators=[
                                    MinValueValidator(1)
                                ])
    count = models.IntegerField('數量', default=0)

    def __str__(self):
        return self.name if self.item is None else self.item.name

    class Meta:
        verbose_name = '維修單細項'
        verbose_name_plural = '維修單細項'
