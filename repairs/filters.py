from django_filters import FilterSet, CharFilter, NumberFilter, DateFilter
from .models import Recorder
from django import forms


class RecorderFilter(FilterSet):
    moto__id = CharFilter(
        label='車牌號碼', field_name='moto__id', lookup_expr='icontains')
    time__start = DateFilter(label='開始時間', field_name='time',
                             lookup_expr='gte', widget=forms.DateInput(attrs={'type': 'date'}))
    time__end = DateFilter(label='結束時間', field_name='time',
                           lookup_expr='lte', widget=forms.DateInput(attrs={'type': 'date'}))
    custom__phone = CharFilter(
        label='客戶手機', field_name='custom__phone', lookup_expr='startswith')

    class Meta:
        model = Recorder
        fields = ['moto__id', 'custom__phone', 'time__start', 'time__end']
