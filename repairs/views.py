from collections import defaultdict
from calendar import monthrange
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages as m

from .forms import (RepairFormSet,
                    CreateRecorderForm,
                    ReportSearchForm,
                    ItemCreateForm,
                    ItemUpdateForm,
                    RootItemCreateForm,
                    RootItemUpdateForm,
                    CategoryCreateForm)
from .models import Recorder, Category, Item, Detail
from .filters import RecorderFilter
from moto.models import Moto
from core.forms import DeleteConfirmForm


@login_required
def index(request):
    _filter = RecorderFilter(
        request.GET or None,
        queryset=Recorder.objects.all()
    )
    paginator = Paginator(_filter.qs.order_by('-id'), 10)
    page = request.GET.get('page')
    create_form = CreateRecorderForm()
    try:
        page = paginator.page(page)
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    return render(request,
                  'repair/index.html',
                  {'filter': _filter,
                   'page': page,
                   'create_form': create_form})


@login_required
def new(request):
    if request.method == "POST":
        moto, _ = Moto.objects.get_or_create(pk=request.POST['id'].upper())
        recorder = Recorder.objects.create(moto=moto)
        return redirect('repair:edit', pk=recorder.pk)
    return redirect('repair:index')


@login_required
def edit(request, pk):
    categories = Category.objects.all()
    recorder = Recorder.objects.get(pk=pk)
    _sum = sum(
        [detail.price*detail.count for detail in recorder.detail_set.all()])
    if request.method == "POST":
        km = request.POST.get('km', None)
        if km and km != recorder.km:
            recorder.km = km
            recorder.save()
        formset = RepairFormSet(request.POST, request.FILES, instance=recorder)
        if formset.is_valid():
            formset.save()
        return redirect('repair:edit', pk=pk)
    else:
        formset = RepairFormSet(instance=recorder)
    return render(request, 'repair/edit.html', {
        'formset': formset,
        'object': recorder,
        'categories': categories,
        'select': 1,
        'sum': _sum
    })


@login_required
def delete(request, pk):
    form = DeleteConfirmForm(request.POST or None)
    recorder = get_object_or_404(Recorder, pk=pk)
    objects = []

    for detail in recorder.detail_set.all():
        detail.fields = {
            detail._meta.get_field(f.name).verbose_name.title(): getattr(detail, f.name, None)
            for f in detail._meta.get_fields()
            if f.name in ['name', 'price', 'count']
        }
        objects.append(detail)

    if request.method == "POST":
        if form.is_valid() and form.cleaned_data['check']:
            recorder.delete()
            m.success(request, '刪除成功')
            return redirect('repair:index')

    return render(
        request,
        'shared/delete_confirm.html',
        {
            'form': form,
            'title': '刪除維修單 #{pk}'.format(pk=pk),
            'objects': objects,
        }
    )


@login_required
def add(request, pk):
    _pk = pk
    recorder = get_object_or_404(Recorder, pk=pk)
    if request.method == "POST":
        add_map = defaultdict(dict)
        for key, value in request.POST.items():
            try:
                pk, name = key.split('-')
                add_map[int(pk)].update({name: value})
            except Exception:
                pass

        # Add for recorder
        for item_id, item in add_map.items():
            try:
                if int(item['count']) == 0:
                    continue

                Detail.objects.create(
                    recorder=recorder,
                    item=Item.objects.get(pk=int(item_id)),
                    name=item['name'],
                    price=item['price'],
                    count=item['count']
                )
            except Exception:
                pass

    return redirect('repair:edit', pk=_pk)


@login_required
def report(request):
    now = datetime.now()
    year, month, day = now.year, now.month, now.day
    start_year, start_month, start_day = year, month, day
    end_year, end_month, end_day = year, month, day
    if request.GET.get('year', None) and request.GET['year'] is not None:
        start_year, start_month, start_day = request.GET['year'], 1, 1
        end_year, end_month, end_day = request.GET['year'], 12, 31
    if request.GET.get('month', None) and request.GET['month'] is not None:
        start_month = end_month = request.GET['month']
        start_day = 1
        _, end_day = monthrange(int(start_year), int(request.GET['month']))
    if request.GET.get('day', None) and request.GET['month'] is not None:
        start_day = end_day = request.GET['day']
    time = {
        'time__start': '-'.join([str(data) for data in [start_year, start_month, start_day]]),
        'time__end': '-'.join([str(data) for data in [end_year, end_month, end_day]])
    }
    _filter = RecorderFilter(
        time,
        queryset=Recorder.objects.all()
    )
    search_form = ReportSearchForm(request.GET or None,)
    paginator = Paginator(_filter.qs.order_by('id'), 10)
    page = request.GET.get('page')
    create_form = CreateRecorderForm()
    try:
        page = paginator.page(page)
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    _sum = sum([
        detail.price*detail.count
        for recorder in _filter.qs
        for detail in recorder.detail_set.all()
    ])
    return render(request, 'repair/report.html', {
        'filter': _filter,
        'page': page,
        'create_form': create_form,
        'sum': _sum,
        'search': search_form
    })


@login_required
def items_detail(request, pk):
    item = get_object_or_404(Item, pk=pk)
    if item.root is None:
        qs = item.item_set.all()
        paginator = Paginator(qs.order_by('-id'), 5)
        page = request.GET.get('page')
        initial = {'category': item.category, 'root': item}
        create_form = ItemCreateForm(initial=initial)
        update_form = ItemUpdateForm(initial=initial)
        create_form.fields['root'].queryset = Item.objects.filter(root=None)
        update_form.fields['root'].queryset = Item.objects.filter(root=None)
        try:
            page = paginator.page(page)
        except PageNotAnInteger:
            page = paginator.page(1)
        except EmptyPage:
            page = paginator.page(paginator.num_pages)
        return render(
            request,
            'item/detail.html',
            {'page': page,
             'create_form': create_form,
             'update_form': update_form,
             'item': item}
        )
    return redirect('repair:items_detail', pk=item.root.pk)


@login_required
def items_new(request):
    form = ItemCreateForm(request.POST or None)
    item = form.save()
    m.success(request, '新增項目 {name} 成功'.format(name=item.name))
    if item.root is None:
        return redirect('repair:categories_detail', pk=item.category.pk)
    return redirect('repair:items_detail', pk=item.root.pk)


@login_required
def items_edit(request, pk):
    item = get_object_or_404(Item, pk=pk)
    form = RootItemUpdateForm(request.POST or None, instance=item)
    item = form.save()
    m.success(request, '修改項目成功')
    if item.root is None:
        return redirect('repair:categories_detail', pk=item.category.pk)
    return redirect('repair:items_detail', pk=item.root.pk)


@login_required
def items_delete(request, pk):
    item = get_object_or_404(Item, pk=pk)
    item.delete()
    m.success(request, '刪除項目 {name} 成功'.format(name=item.name))
    if item.root is None:
        return redirect('repair:categories_detail', pk=item.category.pk)
    return redirect('repair:items_detail', pk=item.root.pk)


@login_required
def categories_index(request):
    qs = Category.objects.all()
    paginator = Paginator(qs.order_by('-id'), 10)
    page = request.GET.get('page')
    create_form = CategoryCreateForm()
    try:
        page = paginator.page(page)
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    return render(request, 'category/index.html',
                  {
                      'page': page,
                      'create_form': create_form,
                  })


@login_required
def categories_detail(request, pk):
    category = Category.objects.get(pk=pk)
    qs = Item.objects.filter(
        root=None,
        category=category
    )
    paginator = Paginator(qs.order_by('-id'), 5)
    page = request.GET.get('page')
    create_form = RootItemCreateForm(initial={'category': category})
    update_form = RootItemUpdateForm(initial={'category': category})
    try:
        page = paginator.page(page)
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    return render(
        request,
        'category/detail.html',
        {
            'page': page,
            'create_form': create_form,
            'update_form': update_form,
            'category': category
        }
    )


@login_required
def categories_new(request):
    if request.method == "POST":
        form = CategoryCreateForm(request.POST or None)
        if form.is_valid():
            category = form.save()
            m.success(request, '新增類別 "{name}" 成功'.format(name=category.name))
    return redirect('repair:categories_index')


@login_required
def categories_edit(request, pk):
    category = get_object_or_404(Category, pk=pk)
    origin_name = category.name
    form = CategoryCreateForm(request.POST or None, instance=category)
    category = form.save()
    m.success(request,
              '修改類別名稱 {origin} -> {now} 成功'.format(
                  origin=origin_name,
                  now=category.name
              ))
    return redirect('repair:categories_index')


@login_required
def categories_delete(request, pk):
    category = get_object_or_404(Category, pk=pk)
    category.delete()
    m.success(request, '刪除成功')
    return redirect('repair:categories_index')
