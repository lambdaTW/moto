# Generated by Django 2.0.6 on 2018-07-08 03:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('repairs', '0007_auto_20180708_1042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='detail',
            name='item',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='repairs.Item'),
        ),
    ]
