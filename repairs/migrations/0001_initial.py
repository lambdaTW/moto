# Generated by Django 2.0.6 on 2018-07-01 12:05

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='類別名稱')),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='項目名稱')),
                ('price', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)], verbose_name='價格')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='repairs.Category')),
                ('root', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='repairs.Item')),
            ],
        ),
    ]
