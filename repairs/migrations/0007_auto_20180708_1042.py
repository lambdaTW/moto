# Generated by Django 2.0.6 on 2018-07-08 02:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('repairs', '0006_auto_20180708_0942'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recorder',
            name='time',
            field=models.DateField(auto_now_add=True),
        ),
    ]
