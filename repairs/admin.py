from django.contrib import admin
from .models import Category, Item, Recorder, Detail

admin.site.register(Category)
admin.site.register(Item)
admin.site.register(Recorder)
admin.site.register(Detail)
