from django.forms import Form, fields

class CustomForm(Form):
    phone = fields.CharField(label='車主號碼', required=False)
    name = fields.CharField(label='車主姓名', required=False)