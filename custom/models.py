from django.db import models


class Custom(models.Model):
    phone = models.CharField('號碼', max_length=20, primary_key=True, default='')
    uid = models.CharField('身份證字號', max_length=20, blank=True, null=True)
    name = models.CharField('姓名', blank=True, null=True, max_length=20)
    address = models.CharField('地址', max_length=150, blank=True, null=True)

    def __str__(self):
        return '-'.join([str(self.phone), str(self.name)])

    class Meta:
        verbose_name = '客戶'
        verbose_name_plural = '客戶'
